<?php
include_once 'vendor/autoload.php';

use \Pondit\Vehicle\LandVehicle\Car;
use \Pondit\Vehicle\LandVehicle\Truck;
use \Pondit\Vehicle\WaterVehicle\Ship;
use \Pondit\Vehicle\AirVehicle\Plane;
use \Pondit\Vehicle\AirVehicle\Helicopter;

$car1 = new Car();
$truck1 = new Truck();
$ship1 = new Ship();
$plane1 = new Plane();
$helicopter1 = new Helicopter();

var_dump($car1);
var_dump($truck1);
var_dump($ship1);
var_dump($plane1);
var_dump($helicopter1);
